# Features I added
- A function that creates new users
- Storing users in the database
- Encrypting and storing passwords in the database
- A login system which checks password agains the encrypted password in the database
- A logout system

# How to test/demo
I  have already set up 2 accounts. These are their login details:
- (Ola, 123)
- (Kari, 123)

If you want to test the security, I have a third account with a secret password:
- (Per, REDACTED)

You can also create new accounts by running 'create_user(username, password)'

# Technical implementation details
The first security feature I added was encrypting the passwords before adding them to the database. I use the
cryptography libray to both encrypt and hash the passwords.

As Flask configures Jinja2 to automatically escape all values, Cross-Site Scripting is not concern unless we
are generating HTML without using Jinja2. In our case, Jinja2 takes care of all Cross-Site Scripting concerns. 

# Threat model – who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?
In terms of confidentiality, all of the messages can only be seen as long as you're logged in. An attacker
would need access to an account to view messages.

In terms of integrity, no data is modified by the application. It is only possible to send a message or view
messages. To modify data, an attacker would need to perform an SQL injection. As my input is sanitized, this
should not be possible.

In terms of availability, the application has not been set up in a production environment. Possible
availability concerns like protecting against DDoS or rebooting after a crash can be set up where the server
will be hosted. Flask auto restarts the program after a crash. Other than that, I sanitize inputs to prevent
an attacker from being able to crash the application.

# What are the main attack vectors for the application?
SQL injection, cross-site scripting attack, cross-site request forgery and insecure design are the main
attack vectors for the application.

# What should we do (or what have you done) to protect against attacks?
Sanitize all user input, encrypt data, refactor codebase to a more secure design, and only use safe libraries.

# What is the access control model?
I have not locked any content except for users that do not have an account, so I would call my design a
role-based access control model with 1 role. 

# How can you know that you security is good enough? (traceability)
Penetration testing and following secure by design standards.

